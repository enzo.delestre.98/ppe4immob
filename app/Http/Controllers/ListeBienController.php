<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\Annonce;


class ListeBienController extends Controller
{
  public function index(){
    $posts = Annonce::all();
    return view('listeBien',compact('posts'));
  }
}
