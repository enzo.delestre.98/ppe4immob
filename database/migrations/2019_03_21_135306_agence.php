<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Agence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Agence', function (Blueprint $table) {
            $table->string('CODE_SOCIETE')->index();
            $table->string('CODE_SITE');
            $table->string('RAISON_SOCIALE');
            $table->string('ENSEIGNE');
            $table->string('CIVILITE');
            $table->string('NOM');
            $table->string('PRENOM');
            $table->string('ADRESSES');
            $table->string('SUITE_ADRESSE');
            $table->string('CP');
            $table->string('VILLE');
            $table->string('PAYS');
            $table->string('TEL');
            $table->string('FAX');
            $table->string('MOBILE');
            $table->string('MAIL');
            $table->string('FORME_JUR');
            $table->string('SIRET');
            $table->string('NO_CARTEPRO_T');
            $table->string('NO_CARTEPRO_G');
            $table->string('GROUPE');
            $table->string('SYNDICAT');
            $table->string('CAISSE_GARANTIE');
            $table->float('CAPITAL_SOCIAL');
            $table->float('MONTANT_GARANTIE_T');
            $table->float('MONTANT_GARANTIE_G');
            $table->date('DATE_CREAT');
            $table->date('DATE_MODIF');
            $table->integer('NO_ASP');
            $table->string('RESEAU');
            $table->string('WEB');
            $table->string('INFO_JUR');
            $table->string('SECONDE_DEVISE');
            $table->float('TX_CONV_SEC_DEVISE');
            $table->string('TEL_LOC');
            $table->string('MAIL_LOC');
            $table->string('PREF_CARTE_PRO');
            $table->string('ADR_SS');
            $table->string('SUITE_ADR_SS');
            $table->string('CP_SS');
            $table->string('VILLE_SS');
            $table->string('PAYS_SS');
            $table->string('ADR_GARANT');
            $table->string('SUITE_ADR_GARANT');
            $table->string('CP_GARANT');
            $table->string('VILLE_GARANT');
            $table->boolean('GRANTIE_MF');
            $table->integer('TYPE_CARTE_PRO');
            $table->integer('TYPE_GARANTIE');
            $table->string('NO_TVA_INTRA');
            $table->string('ADR_PREF');
            $table->string('SUITE_ADR_PREF');
            $table->string('CP_PREF');
            $table->string('VILLE_PREF');
            $table->float('LAT');
            $table->float('LON');
            $table->integer('ID_TIERS');
            $table->string('NOM_GARANTIE_G');
            $table->string('ADR_GARANTIE_G');
            $table->string('SUITE_ADR_GARANTIE_G');
            $table->string('CP_GARANTIE_G');
            $table->string('VILLE_GARANTIE_G');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
