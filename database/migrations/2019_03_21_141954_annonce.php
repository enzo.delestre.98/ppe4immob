<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Annonce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Annonce', function(Blueprint $table){
          $table->increments('id');
           $table->integer('NO_ASP')->nullable();
           $table->string('CODE_SOCIETE')->nullable();
           $table->string('CODE_SITE')->nullable();
           $table->string('NO_DOSSIER')->nullable();
           $table->string('NO_RUE')->nullable();
           $table->string('TYPE_RUE')->nullable();
           $table->string('NO_MANDAT')->nullable();
           $table->string('TYPE_MANDAT')->nullable();
           $table->string('ADR')->nullable();
           $table->string('SUITE_ADR')->nullable();
           $table->string('CP')->nullable();
           $table->string('VILLE')->nullable();
           $table->string('PAYS')->nullable();
           $table->string('QUARTIER')->nullable();
           $table->string('RESIDENCE')->nullable();
           $table->string('NOM_PARK')->nullable();
           $table->string('TRANSPORT')->nullable();
           $table->string('PROXIMITE')->nullable();
           $table->string('SITUATION')->nullable();
           $table->string('TBL')->nullable();
           $table->string('CP_WEB')->nullable();
           $table->string('VILLE_WEB')->nullable();
           $table->string('MODE_CHAUFF')->nullable();
           $table->string('NOM_GEST')->nullable();
           $table->string('REG_FISC')->nullable();
           $table->string('FORM_JUR')->nullable();
           $table->string('CAT')->nullable();
           $table->string('TYPE_CHAUFF')->nullable();
           $table->string('NAT_CHAUFF')->nullable();
           $table->string('TYPE_CUIS')->nullable();
           $table->string('TYPE_BAIL_OCC')->nullable();
           $table->string('STANDING')->nullable();
           $table->string('COUV')->nullable();
           $table->string('FACADE')->nullable();
           $table->string('ETAT_EXT')->nullable();
           $table->string('EAU_CHAUDE')->nullable();
           $table->string('CERTIF_CAR')->nullable();
           $table->string('CERTIF_AM')->nullable();
           $table->string('CERTIF_PB')->nullable();
           $table->string('CERTIF_TER')->nullable();
           $table->string('CE')->nullable();
           $table->string('ACTIVITE')->nullable();
           $table->string('RASON_SOC')->nullable();
           $table->string('ENSEIGNE')->nullable();
           $table->string('URL_VIDEO')->nullable();
           $table->string('ASPECT')->nullable();
           $table->string('TYPE_BAIL')->nullable();
           $table->string('TYPE_CONS')->nullable();
           $table->string('ETAT_GEN')->nullable();
           $table->float('PV')->nullable();
           $table->float('HONO')->nullable();
           $table->float('TX_HONO')->nullable();
           $table->float('CAP_SOC')->nullable();
           $table->float('TAXE_HAB')->nullable();
           $table->float('TAXE_BUR')->nullable();
           $table->float('TAXE_FONC')->nullable();
           $table->float('TAXE_PROF')->nullable();
           $table->float('MAS_SAL')->nullable();
           $table->float('CHARGES')->nullable();
           $table->string('REF_ADB')->nullable();
           $table->string('REGIME_BAIL')->nullable();
           $table->string('TYPE_CAUTION')->nullable();
           $table->float('LOYER_HC')->nullable();
           $table->float('LOYER_PARK')->nullable();
           $table->float('PROV_CH')->nullable();
           $table->float('PROV_CHAUFF')->nullable();
           $table->float('TVA')->nullable();
           $table->float('FRAIS_DIV')->nullable();
           $table->float('LOYER_TTC')->nullable();
           $table->float('LOYER_CC')->nullable();
           $table->float('LOYER')->nullable();
           $table->float('CAUTION_BIP')->nullable();
           $table->float('TRAVAUX')->nullable();
           $table->float('DEP_G')->nullable();
           $table->float('HRAL')->nullable();
           $table->float('SURF_HAB')->nullable();
           $table->float('HRAP')->nullable();
           $table->float('HRATTC')->nullable();
           $table->float('SURF')->nullable();
           $table->float('SURF_CARR')->nullable();
           $table->float('SURF_JAR')->nullable();
           $table->float('SURF_PROF')->nullable();
           $table->float('SURF_ANNEXE')->nullable();
           $table->float('SURF_TERR')->nullable();
           $table->float('TX_TVA')->nullable();
           $table->float('LONG_FAC')->nullable();
           $table->float('COS')->nullable();
           $table->float('CHARGES_ANN')->nullable();
           $table->float('LOYER_ANN')->nullable();
           $table->float('SURF_TOT')->nullable();
           $table->float('SURF_MINI')->nullable();
           $table->float('SURF_CAD')->nullable();
           $table->integer('TYPE_BIEN')->nullable();
           $table->integer('ETAT_AVANC')->nullable();
           $table->integer('NB_PCE')->nullable();
           $table->integer('NB_CHB')->nullable();
           $table->integer('ETAGE')->nullable();
           $table->integer('NIVEAU')->nullable();
           $table->integer('NB_LOT')->nullable();
           $table->integer('NB_ETAGE')->nullable();
           $table->integer('NB_CHB_RDC')->nullable();
           $table->integer('NB_MUR_MIT')->nullable();
           $table->integer('NB_WC')->nullable();
           $table->integer('NB_SDB')->nullable();
           $table->integer('NB_SE')->nullable();
           $table->integer('NB_EMP')->nullable();
           $table->integer('ANNEE1')->nullable();
           $table->integer('ANNEE2')->nullable();
           $table->integer('ANNEE3')->nullable();
           $table->integer('NB_PARK_INT')->nullable();
           $table->integer('NB_PARK_EXT')->nullable();
           $table->integer('NB_BOX')->nullable();
           $table->integer('NB_GAR')->nullable();
           $table->integer('TYPE_STAT')->nullable();
           $table->integer('TYPE_ACCES')->nullable();
           $table->integer('ACCES_PIETON')->nullable();
           $table->integer('NB_VOITURE')->nullable();
           $table->integer('ANNEE_CONS')->nullable();
           $table->integer('LOYER_MT')->nullable();
           $table->integer('DISPO')->nullable();
           $table->integer('NB_CAVE')->nullable();
           $table->integer('NB_BALCON')->nullable();
           $table->boolean('ASCE')->nullable();
           $table->boolean('LOTISS')->nullable();
           $table->boolean('HAND')->nullable();
           $table->boolean('INTERPHONE')->nullable();
           $table->boolean('DIGICODE')->nullable();
           $table->string('REGL_LOYER')->nullable();
           $table->float('LAM_MIN')->nullable();
           $table->float('LAM_MAX')->nullable();
           $table->float('F_HONO')->nullable();
           $table->float('CMA_MIN')->nullable();
           $table->float('CMA_MAX')->nullable();
           $table->float('LOYER_AN_HTHC')->nullable();
           $table->float('TBMA')->nullable();
           $table->float('TFMA')->nullable();
           $table->float('LPIA')->nullable();
           $table->float('LPEA')->nullable();
           $table->float('REPRISE')->nullable();
           $table->boolean('ALARME')->nullable();
           $table->boolean('U_PRO')->nullable();
           $table->boolean('U_BUR')->nullable();
           $table->boolean('U_COMM')->nullable();
           $table->boolean('U_ACT')->nullable();
           $table->boolean('U_PROF')->nullable();
           $table->boolean('BIP')->nullable();
           $table->boolean('CARTE_MAGN')->nullable();
           $table->boolean('VIDEO_SURV')->nullable();
           $table->boolean('CAUTION')->nullable();
           $table->boolean('OCC_PROP')->nullable();
           $table->boolean('CC')->nullable();
           $table->boolean('MC')->nullable();
           $table->boolean('PISCINE')->nullable();
           $table->boolean('MEUBLE')->nullable();
           $table->boolean('TELESURV')->nullable();
           $table->boolean('VIAB')->nullable();
           $table->boolean('GAZ')->nullable();
           $table->boolean('ASS')->nullable();
           $table->boolean('ALIGN')->nullable();
           $table->boolean('CLOT')->nullable();
           $table->date('DATE_CREATION')->nullable();
           $table->date('DATE_MODIF')->nullable();
           $table->date('DATE_MAND')->nullable();
           $table->date('DATE_FIN_MAND')->nullable();
           $table->date('DATE_LIBER')->nullable();
           $table->date('DATE_CREAT_FDC')->nullable();
           $table->date('DATE_MODIF_PRIX')->nullable();
           $table->date('DATE_CAR')->nullable();
           $table->date('DATE_AM')->nullable();
           $table->date('DATE_TER')->nullable();
           $table->date('DATE_CERTIF_URB')->nullable();
           $table->date('DATE_CERTIF_AM')->nullable();
           $table->date('DATE_CONGE')->nullable();
           $table->date('DATE_BAIL')->nullable();
           $table->date('DATE_DISP')->nullable();
           $table->integer('TXT_INTERNET')->nullable();
           $table->integer('TXT_PRESSE')->nullable();
           $table->string('ETAT_INT')->nullable();
           $table->integer('NB_TERR')->nullable();
           $table->integer('NB_NIV')->nullable();
           $table->float('TX_TAXE_PROF')->nullable();
           $table->integer('NB_CHB_SERV')->nullable();
           $table->float('CDAB')->nullable();
           $table->integer('DUREE_BAIL')->nullable();
           $table->float('SURF_SEJ')->nullable();
           $table->float('SURF_DEP')->nullable();
           $table->string('SS')->nullable();
           $table->float('VIAGER_AGE1')->nullable();
           $table->float('BOUQUET')->nullable();
           $table->float('RENTE')->nullable();
           $table->float('SURF_HABITATION')->nullable();
           $table->float('SURF_COMM')->nullable();
           $table->float('SURF_HO')->nullable();
           $table->float('REV_B_AN')->nullable();
           $table->float('REV_HAB')->nullable();
           $table->float('REV_COMM')->nullable();
           $table->float('LONGUEUR')->nullable();
           $table->float('LARGEUR')->nullable();
           $table->float('HAUTEUR')->nullable();
           $table->boolean('GARDIEN')->nullable();
           $table->boolean('GARDIENNAGE')->nullable();
           $table->boolean('PREST')->nullable();
           $table->boolean('VIAGER')->nullable();
           $table->boolean('VIAGER_LIB')->nullable();
           $table->boolean('INSTALL_GAZ')->nullable();
           $table->float('SURF_LOG')->nullable();
           $table->float('LON_VIT')->nullable();
           $table->float('TAUX_TAXE_PROF')->nullable();
           $table->float('CA_AN1')->nullable();
           $table->float('CA_AN2')->nullable();
           $table->float('CA_AN3')->nullable();
           $table->float('BIC_AN1')->nullable();
           $table->float('BIC_AN2')->nullable();
           $table->float('BIC_AN3')->nullable();
           $table->float('SAL_EXPLOIT')->nullable();
           $table->float('VAL_STOCK')->nullable();
           $table->float('CASH_FLOW')->nullable();
           $table->integer('NON_DPE')->nullable();
           $table->string('INFO_BALCON')->nullable();
           $table->string('INFO_TERR')->nullable();
           $table->float('PROF')->nullable();
           $table->float('HAUT_MAX')->nullable();
           $table->float('SURF_PLANCH')->nullable();
           $table->boolean('IMM_COL')->nullable();
           $table->boolean('IMM_INDEP')->nullable();
           $table->boolean('CLIM')->nullable();
           $table->boolean('RIE')->nullable();
           $table->boolean('IFB')->nullable();
           $table->string('NUM_EMP')->nullable();
           $table->string('NO_PRIV')->nullable();
           $table->float('HONO_LOC')->nullable();
           $table->float('HONO_LOC_TTC')->nullable();
           $table->float('TOT_HONO_LOC_TTC')->nullable();
           $table->boolean('NET_TAXE')->nullable();
           $table->boolean('NO_CDAB')->nullable();
           $table->string('SECTEUR')->nullable();
           $table->integer('CRIT_COMP')->nullable();
           $table->float('SURF_JARD')->nullable();
           $table->float('SURF_UTILE')->nullable();
           $table->float('SURF_DEV')->nullable();
           $table->float('VIAGER_AGE2')->nullable();
           $table->integer('DESSERTE')->nullable();
           $table->float('HSP')->nullable();
           $table->float('SURF_BUR')->nullable();
           $table->date('DATE_FIN_BAIL')->nullable();
           $table->integer('MANDAT_ALG')->nullable();
           $table->integer('CH_MT')->nullable();
           $table->date('DATE_INIT_PH')->nullable();
           $table->date('DATE_DPE')->nullable();
           $table->date('DATE_ERNT')->nullable();
           $table->date('DATE_GAZ')->nullable();
           $table->date('DATE_PB')->nullable();
           $table->string('PRESTA_DPE')->nullable();
           $table->string('PRESTA_ERNT')->nullable();
           $table->string('PRESTA_GAZ')->nullable();
           $table->string('PRESTA_PB')->nullable();
           $table->string('NOM_CONTACT')->nullable();
           $table->string('TEL_CONTACT')->nullable();
           $table->string('DIST')->nullable();
           $table->integer('CH_HONO')->nullable();
           $table->float('TOTAL_HONO_TTC')->nullable();
           $table->string('DPE')->nullable();
           $table->string('GES')->nullable();
           $table->float('VAL_DPE')->nullable();
           $table->float('VAL_GES')->nullable();
           $table->boolean('CPG_LAF')->nullable();
           $table->boolean('INSTALL_ELEC')->nullable();
           $table->date('DATE_ELEC')->nullable();
           $table->string('PRESTA_ELEC')->nullable();
           $table->float('TOTAL_HONO_HT')->nullable();
           $table->boolean('ALI')->nullable();
           $table->float('LAT')->nullable();
           $table->float('LON')->nullable();
           $table->date('DATE_DIANC')->nullable();
           $table->string('PRESTA_DIANC')->nullable();
           $table->boolean('TAE')->nullable();
           $table->boolean('PRIX_CONF')->nullable();
           $table->integer('MOTS_CLES')->nullable();
           $table->integer('COPROPRIETE')->nullable();
           $table->integer('NB_LOTS_COPRO')->nullable();
           $table->float('MONTANT_QUOTEPART')->nullable();
           $table->integer('PROCEDURE_SYND')->nullable();
           $table->string('PROCEDURE_DETAIL')->nullable();
           $table->integer('INDICE_NATURE_CHARGE')->nullable();
           $table->integer('COMPLEMENT_LOYER')->nullable();
           $table->integer('INDICE_REGLEMENT_LOYER')->nullable();
           $table->integer('INDICE_REGLEMENT_CHARGES')->nullable();
           $table->float('HONO_ETAT_LIEU_LOC')->nullable();
           $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
       });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
