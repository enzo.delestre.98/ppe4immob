<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Piece extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Piece', function (Blueprint $table) {
            $table->string('DESIGNATION')->index();
            $table->string('NAT_SOL');
            $table->string('RAISON_SOCIALE');
            $table->string('VUE');
            $table->string('EXPO');
            $table->string('COMMENTAIRES');
            $table->string('NO_ORDRE');
            $table->float('SURFACE');
            $table->float('HSP');
            $table->integer('NIVEAU');
            $table->integer('NO_ASP');
            $table->integer('NO_ASP_PHOTO');   
    });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
