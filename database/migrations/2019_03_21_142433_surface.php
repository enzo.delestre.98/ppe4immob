<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Surface extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Surface', function (Blueprint $table) {
            $table->string('NAT')->index();
            $table->string('VUE');
            $table->string('COMMENTAIRES');
            $table->string('NO_ORDRE');
            $table->float('SURFACE');
            $table->float('HSP');
            $table->integer('ETAGE');
            $table->integer('NO_ASP');
            $table->float('LOYER_M2');
            $table->float('SURF_MINI');
            $table->float('CHARGES_M2');
    });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        //
    }
}
