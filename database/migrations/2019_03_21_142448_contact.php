<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Contact', function (Blueprint $table) {
            $table->string('CIVILITE')->index();
            $table->string('NOM');
            $table->string('PRENOM');
            $table->string('ADRESSE');
            $table->string('SUITE_ADRESSE');
            $table->string('CP');
            $table->string('VILLE');
            $table->string('PAYS');
            $table->string('TEL');
            $table->string('FAX');
            $table->string('MOBILE');
            $table->string('MAIL');
            $table->integer('NO_ASP');
            $table->string('LIBELLE');
            $table->string('FONCTION');
            $table->string('URL_PHOTO');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
