<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Lot', function (Blueprint $table) {
            $table->string('CAT_APPT')->index();
            $table->string('NO_LOT');    
            $table->string('BAIL');
            $table->string('COMMENTAIRES');
            $table->string('NO_ORDRE');
            $table->float('SURFACE');
            $table->float('LOYER_ANN');
            $table->integer('ETAGE');
            $table->integer('NO_ASP');
            $table->integer('NO_ASP_PHOTO');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
