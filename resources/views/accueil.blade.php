<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

        </style>
    </head>
    <body>

      <a href="{{ url('/listeBien')}}"><button type="button" class="btn btn-success btn-lg btn-block">LISTE DES BIENS</button></a>
      <a href="{{ url('/exportBien')}"><button type="button" class="btn btn-danger btn-lg btn-block">EXPORT DE BIENS</button></a>
      <a href="{{ url('/compteRenduBien')}"><button type="button" class="btn btn-warning btn-lg btn-block">COMPTE RENDU DE VISITE</button></a>    

    </body>
</html>
