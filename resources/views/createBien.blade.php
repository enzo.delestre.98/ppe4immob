<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .content-formulaire{
              text-align: center;
              width: 50%;
              margin: 0 auto;
              margin-top: 50vh;
              transform: translateY(-50%);
            }
            input{
              margin-top: 10px;
              margin-bottom: 10px;
              border-radius: 10px;
              border: 1px solid grey;
              padding: 10px;
            }
            .form-row{
              width: 100%;
              display: flex;
              flex-wrap: wrap;
            }
            .form-group{
              width: 50%;
            }
            .btn{
              width: 50%;
              padding: 10px;
              border-radius: 20px;
              border: none;
              color: white;
              background-color:#3498db;
            }

        </style>
    </head>
    <body>



      <div class="content-formulaire">
        <h1>Création d'un bien</h1>
        <form method="post">

          {{ csrf_field() }}



          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">NO_ASP</label>
              <input class="col-form-label col-sm-2" type="number" name="no_asp" placeholder="NO_ASP">
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">LOYER</label>
              <input class="col-form-label col-sm-2" type="number" name="loyer" placeholder="LOYER">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group">
              <label for="inputAddress">SURF_HABITATION</label>
              <input class="col-form-label col-sm-2" type="number" name="surf_habitation" placeholder="SURF_HABITATION">
            </div>
            <div class="form-group">
              <label for="inputAddress2">NO_RUE</label>
              <input class="col-form-label col-sm-2" type="text" name="no_rue" placeholder="NO_RUE">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">VILLE</label>
              <input class="col-form-label col-sm-2" type="text" name="ville" placeholder="VILLE">
            </div>
            <div class="form-group col-md-4">
              <label for="inputState">CP</label>
              <input class="col-form-label col-sm-2" type="number" name="cp" placeholder="CP">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="inputZip">NOM_GEST</label>
              <input class="col-form-label col-sm-2" type="text" name="nom_gest" placeholder="NOM_GEST">
            </div>
            <div class="form-group col-md-2">
              <label for="inputZip">SUITE_ADR</label>
              <input class="col-form-label col-sm-2" type="text" name="suite_adr" placeholder="SUITE_ADR">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="inputZip">TVA</label>
                <input class="col-form-label col-sm-2" type="number" name="tva" placeholder="TVA">
            </div>
            <div class="form-group col-md-2">
              <label for="inputZip">MEUBLE</label>
              <input class="col-form-label col-sm-2" type="text" name="meuble" placeholder="MEUBLE">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="inputZip">CHARGES</label>
              <input class="col-form-label col-sm-2" type="text" name="charges" placeholder="CHARGES">
            </div>
            <div class="form-group col-md-2">
              <label for="inputZip">QUARTIER</label>
              <input class="col-form-label col-sm-2" type="text" name="quartier" placeholder="QUARTIER">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="inputZip">RESIDENCE</label>
              <input class="col-form-label col-sm-2" type="text" name="residence" placeholder="RESIDENCE">
            </div>
            <div class="form-group">
              <label for="inputZip">TYPE_CHAUFF</label>
              <input class="col-form-label col-sm-2" type="text" name="type_chauff" placeholder="TYPE_CHAUFF">
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Valider</button>
        </form>
      </div>
    </body>
</html>
