<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            th{
              border: 1px solid grey;
            }

        </style>
    </head>
    <body>
      @extends('layout')
      @section('contenu')
      <div class="container-formulaire">

        <table class="table table-striped">
          <thead>
            <th>NO_ASP</th>
            <th>LOYER</th>
            <th>SURF_HABITATION</th>
            <th>NO_RUE</th>
            <th>VILLE</th>
            <th>CP</th>
            <th>NOM_GEST</th>
            <th>TYPE_CHAUFF</th>
            <th>SUITE_ADR</th>
            <th>TVA</th>
            <th>MEUBLE</th>
            <th>CHARGES</th>
            <th>QUARTIER</th>
            <th>RESIDENCE</th>
          </thead>
          @foreach($posts as $value)
            <tr>
              <td scope="row"> {{$value->NO_ASP}}</td>
              <td>{{$value->LOYER}}</td>
              <td>{{$value->SURF_HABITATION}}</td>
              <td>{{$value->NO_RUE}}</td>
              <td>{{$value->VILLE}}</td>
              <td>{{$value->CP}}</td>
              <td>{{$value->NOM_GEST}}</td>
              <td>{{$value->TYPE_CHAUFF}}</td>
              <td>{{$value->SUITE_ADR}}</td>
              <td>{{$value->TVA}}</td>
              <td>{{$value->MEUBLE}}</td>
              <td>{{$value->CHARGES}}</td>
              <td>{{$value->QUARTIER}}</td>
              <td>{{$value->RESIDENCE}}</td>
            </tr>

            @endforeach
        </table>
      @endsection
      </div>

    </body>
</html>
