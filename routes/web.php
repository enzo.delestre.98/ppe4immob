<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/createBien', function(){
  return view('createBien');
});

Route::post('/createBien', function(){


  $utilisateur = new App\Annonce;
  $utilisateur->NO_ASP = request('no_asp');
  $utilisateur->LOYER = request('loyer');
  $utilisateur->SURF_HABITATION = request('surf_habitation');
  $utilisateur->NO_RUE = request('no_rue');
  $utilisateur->VILLE = request('ville');
  $utilisateur->CP = request('cp');
  $utilisateur->NOM_GEST = request('nom_gest');
  $utilisateur->TYPE_CHAUFF = request('type_chauff');
  $utilisateur->SUITE_ADR = request('suite_adr');
  $utilisateur->TVA = request('tva');
  $utilisateur->MEUBLE = request('meuble');
  $utilisateur->CHARGES = request('charges');
  $utilisateur->QUARTIER = request('quartier');
  $utilisateur->RESIDENCE = request('residence');


$utilisateur->save();

  return 'Formulaire reçu';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/accueil', 'AccueilController@index')->name('accueil');
Route::get('/listeBien', 'ListeBienController@index')->name('listeBien');
Route::get('/createBien', 'CreateBienController@index')->name('createBien');

// Route::get('/listeBien', function() { return App\Annonce::all(); });
